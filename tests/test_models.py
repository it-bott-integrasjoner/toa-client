import pytest
from pydantic import ValidationError

from toa_client.models import Person


def test_person_ok(person_ok):
    assert Person.from_dict(person_ok)


def test_person_fail(person_fail):
    with pytest.raises(ValidationError):
        Person.from_dict(person_fail)
