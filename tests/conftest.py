import json

import pytest
import requests_mock

from toa_client.client import ToaClient, ToaEndpoints, load_json_file


@pytest.fixture
def baseurl():
    return "https://localhost"


@pytest.fixture
def endpoints(baseurl):
    return ToaEndpoints(baseurl)


@pytest.fixture
def custom_endpoints(baseurl):
    return ToaEndpoints(
        baseurl,
        "custom/tilsetting%20og%20arbeidskontrakt",
    )


@pytest.fixture
def client(baseurl):
    return ToaClient(baseurl)


@pytest.fixture
def mock_api(client):
    with requests_mock.Mocker() as m:
        yield m


@pytest.fixture()
def person_ok():
    return load_json_file("working_example.json")


@pytest.fixture()
def person_fail():
    return load_json_file("failing_example.json")
