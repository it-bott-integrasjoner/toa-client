from toa_client.client import ToaEndpoints


def test_init(baseurl):
    endpoints = ToaEndpoints(baseurl)
    assert endpoints.baseurl == baseurl


def test_get_person(baseurl, endpoints):
    person_id = "123"
    url = endpoints.get_person(person_id)
    assert url == baseurl + "/tilsetting%20og%20arbeidskontrakt/" + person_id


def test_custom_get_person(custom_endpoints, baseurl):
    person_id = "123"
    assert (
        custom_endpoints.get_person(person_id)
        == baseurl + "/custom/tilsetting%20og%20arbeidskontrakt/" + person_id
    )
