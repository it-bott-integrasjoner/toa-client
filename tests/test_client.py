import pytest

from toa_client.client import ToaClient


@pytest.fixture
def header_name():
    return "X-Test"


@pytest.fixture
def client_cls(header_name):
    class TestClient(ToaClient):
        default_headers = {
            header_name: "6a9a32f0-7322-4ef3-bbce-6685a3388e67",
        }

    return TestClient


def test_init_does_not_mutate_arg(client_cls, baseurl):
    headers = {}
    client = client_cls(baseurl, headers=headers)
    assert headers is not client.headers
    assert not headers


def test_init_applies_default_headers(client_cls, baseurl, header_name):
    headers = {}
    client = client_cls(baseurl, headers=headers)
    assert header_name in client.headers
    assert client.headers[header_name] == client.default_headers[header_name]


def test_init_modify_defaults(client_cls, baseurl, header_name):
    headers = {header_name: "ede37fdd-a2ae-4a96-9d80-110528425ea6"}
    client = client_cls(baseurl, headers=headers)
    # Check that we respect the headers arg, and don't use default_headers
    assert client.headers[header_name] == headers[header_name]
    # Check that we don't do this by mutating default_headers
    assert client.default_headers[header_name] != headers[header_name]
