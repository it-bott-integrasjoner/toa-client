"""Client for connecting to ToA API"""
import json
import logging
import os
import urllib.parse
from typing import Any, Dict, List, Optional, Tuple, Type, Union

import requests

from toa_client.models import BaseModel_T, Person

logger = logging.getLogger(__name__)

JsonType = Any


def load_json_file(name: str) -> JsonType:
    """Load json file from the fixtures directory"""
    here = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__).rsplit("/", 1)[0])
    )
    with open(os.path.join(here, "tests/fixtures", name)) as f:
        data = json.load(f)
    return data


def urljoin(base_url: str, *paths: Optional[str]) -> str:
    """
    A sane urljoin.

    >>> urllib.parse.urljoin('https://localhost/foo', 'bar')
    'https://localhost/bar'

    >>> urljoin('https://localhost/foo', 'bar')
    'https://localhost/foo/bar'

    >>> urljoin('https://localhost/foo', 'bar', 'baz')
    'https://localhost/foo/bar/baz'
    """
    for path in paths:
        base_url = urllib.parse.urljoin(base_url + "/", path)
    return base_url


def merge_dicts(*dicts: Optional[Dict[str, Any]]) -> Dict[str, Any]:
    """
    Combine a series of dicts without mutating any of them.

    >>> merge_dicts({'a': 1}, {'b': 2})
    {'a': 1, 'b': 2}
    >>> merge_dicts({'a': 1}, {'a': 2})
    {'a': 2}
    >>> merge_dicts(None, None, None)
    {}
    """
    combined = dict()
    for d in dicts:
        if not d:
            continue
        for k in d:
            combined[k] = d[k]
    return combined


class ToaEndpoints:
    def __init__(
        self,
        url: str,
        person_url: Optional[
            str
        ] = "/tilsetting%20og%20arbeidskontrakt",  # This is a guess. There are spaces in the swagger file
    ) -> None:
        self.baseurl = url
        self.person_url = person_url

    """ Get endpoints relative to the ToA API URL. """

    def __repr__(self) -> str:
        return "{cls.__name__}({url!r})".format(cls=type(self), url=self.baseurl)

    def get_person(self, person_id: str) -> str:
        return urljoin(self.baseurl, self.person_url, person_id)


class ToaClient(object):
    default_headers = {
        "Accept": "application/json",
    }

    def __init__(
        self,
        url: str,
        person_url: Union[str, None] = None,
        tokens: Dict[str, str] = None,
        headers: Union[None, Dict[str, Any]] = None,
        return_objects: bool = True,
        use_sessions: bool = True,
    ) -> None:
        """
        ToA API client.

        :param str url: Base API URL
        :param str person_url: Relative URL to person API
        :param dict tokens: Tokens for the different APIs
        :param dict headers: Append extra headers to all requests
        :param bool return_objects: Return objects instead of raw JSON
        :param bool use_sessions: Keep HTTP connections alive (default True)
        """
        self.urls = ToaEndpoints(url, person_url)
        if tokens:
            self.tokens = tokens
        else:
            self.tokens = {}
        self.headers = merge_dicts(self.default_headers, headers)
        self.return_objects = return_objects
        if use_sessions:
            self.session = requests.Session()
        else:
            self.session = requests  # type: ignore[assignment]

    def _build_request_headers(
        self, headers: Optional[Dict[str, str]]
    ) -> Dict[str, Any]:
        request_headers = {}
        for h in self.headers:
            request_headers[h] = self.headers[h]
        for h in headers or ():
            request_headers[h] = headers[h]  # type: ignore[index]
        return request_headers

    def call(
        self,
        method_name: str,
        url: str,
        headers: Optional[Dict[str, Any]] = None,
        params: Optional[Dict[str, Any]] = None,
        return_response: bool = True,
        **kwargs: Any,
    ) -> JsonType:
        headers = self._build_request_headers(headers)
        if params is None:
            params = {}
        logger.debug(
            "Calling %s %s with params=%r",
            method_name,
            urllib.parse.urlparse(url).path,
            params,
        )
        r = self.session.request(
            method_name, url, headers=headers, params=params, **kwargs
        )
        if r.status_code in (500, 400, 401):
            logger.warning("Got HTTP %d: %r", r.status_code, r.content)
        if return_response:
            return r
        r.raise_for_status()
        return r.json()

    def get(self, url: str, **kwargs: Any) -> JsonType:
        return self.call("GET", url, **kwargs)

    def put(self, url: str, **kwargs: Any) -> JsonType:
        return self.call("PUT", url, **kwargs)

    def post(self, url: str, **kwargs: Any) -> JsonType:
        return self.call("POST", url, **kwargs)

    def object_or_data(
        self, cls: Type[BaseModel_T], data: Dict[str, Any]
    ) -> Union[object, Dict[str, Any]]:
        if not self.return_objects:
            return data
        return cls.from_dict(data)

    def get_person(
        self,
        person_id: str,
    ) -> JsonType:
        """Convenience method for updating

        :param person_id: The person or people we want to update in ToA
        :return: String describing status or None if not found
        """
        url = self.urls.get_person(person_id)
        headers = self.headers
        headers.update(self.tokens.get("api_key", {}))  # type: ignore[arg-type]
        response = self.get(url, headers=headers)
        if response.status_code == 404:
            return None
        if response.status_code == 200:
            return Person(**response.json())
        response.raise_for_status()


def get_client(config_dict: Dict[str, Any]) -> ToaClient:
    """
    Get a ToaClient from configuration.
    """
    return ToaClient(**config_dict)
