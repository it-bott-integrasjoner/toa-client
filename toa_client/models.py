"""Models used by the client"""

import json
import typing
from typing import Dict, Any, TypeVar, Union

import pydantic

BaseModel_T = typing.TypeVar("BaseModel_T", bound="BaseModel")


def to_lower_camel(s: str) -> str:
    """Alias generator to avoid breaking PEP8"""
    first, *others = s.split("_")
    return "".join([first.lower(), *map(str.capitalize, others)])


class BaseModel(pydantic.BaseModel):
    """Expanded BaseModel for convenience"""

    @classmethod
    def from_dict(cls: typing.Type[BaseModel_T], data: Dict[str, Any]) -> BaseModel_T:
        """Initialize class from dict"""
        return cls(**data)

    @classmethod
    def from_json(
        cls: typing.Type[BaseModel_T], json_data: Union[str, bytes, bytearray]
    ) -> BaseModel_T:
        """Initialize class from json file"""
        data = json.loads(json_data)
        return cls.from_dict(data)


class Person(BaseModel):
    """Definition of a Person in ToA Api

    This is according to Infokontrakter API_0.9.json
    """

    id: str
    kontraktnr: str
    type_kontrakt: str  # TODO: Is this an enum? See example values in api
    overlapp: str
    kontraktstype: str
    organisasjons_id: str
    kostnadssted: str
    kostnadssted_navn: typing.Optional[str]
    stillingskode: typing.Optional[int]
    stillingstittel: typing.Optional[str]
    yrkeskode: str
    lonnstrinn: typing.Optional[str]
    kronetillegg: typing.Optional[str]
    arslonn: typing.Optional[str]
    dellonnsprosent: str
    timer_fort: typing.Optional[str]
    startdato: typing.Optional[str]
    sluttdato: typing.Optional[str]
    endret_dato: str
    endret_av: str

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True
